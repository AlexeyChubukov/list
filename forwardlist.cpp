
#include <iostream>
#include <vector>
using namespace std;
#include "forwardlist.h"

ForwardList::ForwardList()
		{
			first = nullptr;
		}
ForwardList::ForwardList(Node* first)
		{
			this->first = first;
		}
ForwardList::~ForwardList()
{
	while (first != nullptr)
	{
		Node *node_copy = first->next;
		delete first;
		first = node_copy;
	}
}
	
void ForwardList::push_back(int value)
		{
			Node *p = first;
			if (p == nullptr)
			{
				first = new Node{nullptr,value};
				return;
			}
			while (p->next != nullptr)
			{
				p = p->next;
			}
			p->next = new Node{ nullptr,value };
		}

bool ForwardList::remove(int value)
{
	Node *p = first;
	if (p == nullptr) {
		cout << "Элемент " << value << " отсутствует!" << endl;
		return false;
	}
	if (p->data == value) {
		Node *temp = first;
		first = first->next;
		delete temp;
		return true;
	} else {
		while (p != nullptr) {
			if (p->next == nullptr) {
				cout << "Элемент "<< value<< " отсутствует!" << endl;
				return false;
			}
			if (p->next->data == value) {
				Node *p1 = p->next;
				p->next = p->next->next;
				delete p1;
				return true;
			}
			p = p->next;
		}
	}
}

void ForwardList::print() const
		{
			Node *p = first;
			if (p == nullptr)
			{
				cout << "Список пуст!" << endl;
				return;
			}
			while (p != nullptr)
			{
				if (p->next != nullptr) {
					cout << p->data << " -> ";
				}
				else {
					cout << p->data << endl;
				}
				p = p->next;
			}
		}
void ForwardList::search(int a) const
{
	Node *p = first;
	int i = 0;
	bool is_found = false;
	while (p != nullptr)
	{
		if (p->data == a)
		{
			cout << i << ' ';
			is_found = true;
		}
		p = p->next;
		i++;
	}
	if (!is_found)
	{
		cout << "Элемент не найден" << endl;
	}
}
void ForwardList::replace(int pos, int new_value) const
{
	Node *p = first;
	int i = 0;
	while (p != nullptr)
	{
		if (i == pos)
		{
			p->data = new_value;
			return;
		}
		i++;;
		p = p->next;
	}
	cout << "Элемент с позицией " << pos << " не существует" << endl;
}

void ForwardList::sort() const
{
	Node *p = first;
	Node *ptr = first;
	if (p == nullptr)
	{
		cout << "Список пуст!" << endl;
		return;
	}
	else
	{
		vector<int> v;
		while (p != nullptr)
		{
			v.push_back(p->data);
			p = p->next;
		}
		for (int i = 0; i < v.size(); i++)
		{
			int temp = v[0];
			for (int j = i; j < v.size() - 1; j++)
			{
				if (v[i]>v[j + 1])
				{
					temp = v[i];
					v[i] = v[j + 1];
					v[j + 1] = temp;
				}
			}
		}
		int i = 0;
		while (ptr != nullptr)
		{
			ptr->data = v[i];
			ptr = ptr->next;
			i++;
		}
	}
}
