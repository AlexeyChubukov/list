#ifndef forwardlist_h
#define forwardlist_h

struct Node
{
	Node* next;
	int data;
};

class ForwardList
{
	Node* first;
public:
	ForwardList();
	ForwardList(Node* first);
	~ForwardList();
	void push_back(int value);
	bool remove(int value);
	void print() const;
	void search(int a) const;
	void replace(int pos, int new_value) const;
	void sort() const;
};
#endif
