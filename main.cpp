// lab2.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <vector>
#include <cstring>
#include "forwardlist.h"

using namespace std;

int dlina_number(int num);
void menu();

int main(int argc, char* argv[])
{
#ifdef WIN32
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
#endif
	ForwardList list;
	if (argc ==2)
	{
		char *p = argv[1];
			while (strlen(p) != 0) {
			while (atoi(p) == 0 && p[0] != '0' && strlen(p) != 0) {
				p++;
			}
			list.push_back(atoi(p));
			p += dlina_number(atoi(p));
		}
	}
	if (argc > 2)
	{
		int i = 1;
		list.push_back(atoi(argv[i]));
		while (argc - 1 != i)
		{
			i++;
			list.push_back(atoi(argv[i]));
		}
	}

	bool exit = false;
	while (!exit)
	{
		menu();
		char choose[10]{};
		cin >> choose;
		if (strlen(choose)!=dlina_number(atoi(choose)))
		{
			cout << "Неправильный ввод данных!" << endl;
		}
		else
		{
			switch (atoi(choose))
			{
			case 1:
				list.print();
				break;
			case 2:
			{
				cout << "Введите элементы:" << endl;
				char mas[100]{}, *p = mas;
				cin.ignore();
				cin.getline(mas, 100);
				while (strlen(p)!=0)
				{
					list.push_back(atoi(p));
					p += dlina_number(atoi(p)) + 1;
				}
			}
			break;
			case 3:
			{
				cout << "Введите значение элемента: ";
				char el[10]{};
				cin >> el;
				if (strlen(el) != dlina_number(atoi(el)))
				{
					cout << "Введено не число!" << endl;
				}
				else
				{
					list.remove(atoi(el));
				}
			}
			break;
			case 4:
			{
				cout << "Введите значение элемента: ";
				int el = 0;
				cin >> el;
				list.search(el);
			}
			break;
			case 5:
			{
				cout << "Введите позицию и новое значение: ";
				char arr[10]{}, *p=arr;
				cin.ignore();
				cin.getline(arr,10);
				int position = atoi(p);
				p += dlina_number(atoi(p)) + 1;
				int new_value = atoi(p);
				list.replace(position, new_value);
			}
				break;
			case 6:
				list.sort();
				break;
			case 7:
			{
				bool is_correct = false;
				while (!is_correct)
				{
					cout << "Вы хотите выйти из программы ? (y,N)" << endl;
					string answer;
					cin >> answer;
					if (answer == "y" || answer == "Y" || answer == "yes" || answer == "Yes" || answer == "YES")
					{
						cout << "До свидания!" << endl;
						is_correct = true;
						exit = true;
						break;
					}
					if (answer == "N" || answer == "n" || answer == "no" || answer == "No" || answer == "NO")
					{
						is_correct = true;
						break;
					}
					cout << "Попробуйте еще раз!" << endl;
				}
				break;
			}
			default:
				cout << "Неправильный ввод данных!" << endl;
				break;
			}
		}
		
	}
	
    return 0;
}

int dlina_number(int num)
{
	int count = 0, num_copy=num;
	while (num != 0)
	{
		num /= 10;
		count++;
	}
	if (num_copy < 0 || count==0)
	{
		return ++count;
	}
	return count;
}

void menu()
{
	cout << endl;
	cout << "Выберите одну из операций:" << endl;
	cout << "1. Распечатать список" << endl;
	cout << "2. Добавить элементы в список" << endl;
	cout << "3. Удалить элемент" << endl;
	cout << "4. Найти позиции элементов" << endl;
	cout << "5. Заменить элемент на другой" << endl;
	cout << "6. Отсортировать элементы списка" << endl;
	cout << "7. Завершить работу программы" << endl;
}
